import logging
# Extended logging only in debug mode. If exception occured it should be handled by top level function 

class DoorDriver:

    # Dependency injection - pymodbus instance and door close register to read/write
    def __init__(self,modbusClientInstance,doorModbusRegister):
        #logging module
        self.logger = logging.getLogger('doorDriver')
        self.modbusClient = modbusClientInstance
        self.doorRegister = doorModbusRegister
        pass

    # write value to modbus register
    def writeRegister(self,register,value):
        self.logger.debug("writeRegister register: %d value: %d",register,value)
        rq = self.modbusClient.write_register(register,value)
        # there was an error code in modbus. MSB is set
        # from http://www.simplymodbus.ca/exceptions.htm
        if rq.function_code >= 0x80:
            self.logger.debug("writeRegister invalid req code. %d",rq.function_code)
            raise Exception('writeRegister invalid req code ',rq.function_code)
        return True

    # read value from modbus register
    def readRegister(self,register):
        self.logger.debug("readRegister register %d",register)
        rq = self.modbusClient.read_holding_registers(register)
        #Invalid response
        if rq.function_code >= 0x80:
            self.logger.debug("readRegister invalid req code %d",rq.function_code)
            raise Exception('readRegister invalid req code ',rq.function_code)
        self.logger.debug("readRegister readed registers:  %s",rq.registers)
        return rq.registers
    
    # opening door function
    # return true if register was successfully written. Exception handled by upper layer
    def openDoor(self):
        # Opening door - write 1 to doorRegister
        res = self.writeRegister(self.doorRegister,1)
        if res is True:
            return res
        else:
            raise Exception('openDoor cannot open door')
        

    # TRUE if door is open, FALSE if door is close
    def checkDoorState(self):
        res = self.readRegister(self.doorRegister)
        # assuming 1 means door is opened
        if res[0] == 1:
            return True
        if res[0] == 0:
            return False
        else:
            raise Exception("checkDoorState unsupported door state %s",res[0])

