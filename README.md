
Simple API endpoint to open door using modbus protocol.

# Requirements

* python >3.6
* pymodbus
* flask (HTTP Server)
* docker (environment for running and testing)
* pytest (for testing app)

# Running app

If you want to test application, there is ready to use modbus server simulator. inside `test` directory, simply run:

```
python modbus_server.py
```
It runs modbus server on your `localhost:5020`

After that, run application in root level dir:

```
python main.py
```

It runs simple HTTP server on `localhost:8080`

# API

ROUTE: `/api/open`
METHOD: `GET`
RESPONSES:
 * `200` - door opens correctly
 ```
 {
     "status":"door opened"
 }
 ``` 
 * `500` - error during door opening. You need to check logs
 ```
 {
  "code": 500,
  "description": "The server encountered an internal error and was unable to complete your request. Either the server is overloaded or there is an error in the application.",
  "name": "Internal Server Error"
}
 ```

 # Docker
 You can also build and run `Dockerfile` in main directory. Simply, build and run:

 ```
 docker build . -t repository:name
 docker run -p 8080:8080 -t repository:name
 ```

 # Tests
 You can run tests using `pytest` suite. Inside `/test` run `pytest` command. `test_door_driver` suite should start `modbus_server`, and run test suite. You can also run tests inside docker container. `Dockerfile` is in `/test` directory
 Build it from root directory context like
 ```
 docker build -t repository:tag -f .\test\Dockerfile .
 ```
 
 # CI/CD
 Every commit on `master` branch executes docker with `pytest` suite. It has only one stage
 
 # Additional info
In repo three is `insomnia_env.json` file. You can import environment and test HTTP server
`https://insomnia.rest/`

fix
