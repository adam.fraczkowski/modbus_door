import sys
sys.path.append("..")
import pytest
import subprocess
from pymodbus.client.sync import ModbusTcpClient
from door_driver import DoorDriver
import logging
import time
FORMAT = ('%(asctime)-15s'
          ' %(levelname)-8s %(module)-15s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

# door register
DOOR_REGISTER = 200


@pytest.fixture(scope='session', autouse=True)
def modbusServer():
    global modbusSim
    log.info("Running modbus server")
    modbusSim = subprocess.Popen(['python','modbus_server.py'])
    # time to run modbus server
    time.sleep(1)

@pytest.fixture(scope='session',autouse=True)
def modbusClient():
    return ModbusTcpClient('127.0.0.1', port=5020)

def test_dooropen(modbusServer,modbusClient):
    # initialize door driver
    log.info("Starting test dooropen")
    doorDriver = DoorDriver(modbusClient,DOOR_REGISTER)
    # try to open doors
    log.info("Door opening")
    res = doorDriver.openDoor()
    # check func result
    assert res == True
    # check low level register
    log.info("Checking door register")
    register_res = doorDriver.readRegister(DOOR_REGISTER)
    assert register_res[0] == 1
    # check checkDoorStatus is true
    log.info("Checking door state")
    assert doorDriver.checkDoorState() == True
    modbusSim.terminate()
    
