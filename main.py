from door_driver import DoorDriver
from pymodbus.client.sync import ModbusTcpClient
import os
from flask import Flask, jsonify, json
app = Flask(__name__)
# configure logging level
import logging
from werkzeug.exceptions import HTTPException

FORMAT = ('%(asctime)-15s' ' %(levelname)-8s %(module)-15s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()

# get from env vars
if os.environ.get('DEBUG') == "1":
    log.setLevel(logging.DEBUG)
else:
    log.setLevel(logging.INFO)

PORT = os.environ.get('PORT') or 5020
IPADDR = os.environ.get('IPADDR') or "127.0.0.1"
MODBUS_REGISTER = os.environ.get('MODBUS_REGISTER') or 200

try:
    # initialize modbus client
    client = ModbusTcpClient(os.environ.get('IPADDR'), port=int(PORT))
    # initialize door driver
    doorDriver = DoorDriver(client,int(MODBUS_REGISTER))
except Exception as e:
    log.exception(e)

# generic error handled from: https://flask.palletsprojects.com/en/master/errorhandling/
@app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response

# flask routes
@app.route('/api/open', methods=['GET'])
def open_door():
    #TODO Authentication of request. Request should be authenticated - token or something
    result = doorDriver.openDoor()
    if result is True:
        data = {"status":"door opened"}
        return jsonify(data), 200
    else:
        data = {"status":"error during door open. Check logs for more information"}
        return jsonify(data), 500

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)